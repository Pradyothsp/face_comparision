import os
from shutil import rmtree

from flask import Flask, flash, redirect, request, url_for, send_file
from werkzeug.utils import secure_filename

# files manupulations
from os import listdir
from os.path import isfile, join

try:
    rmtree('temp_folder')
    rmtree('output')
except:
    pass

os.mkdir('temp_folder')
os.mkdir('output')

UPLOAD_FOLDER = 'temp_folder'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    html = '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <style>
    body {
        background-image: url('background_image/photo.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed; 
        background-size: 100% 100%;
    }
    </style>
    <body>
    <form method=POST enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
    </body>
    </form>
    '''
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # save image
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            # now run the detector_final
            import detector_final
            
            detector_final.show_images()

            # now get the image form the output
            mypath = './output'
            files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
            image_html = ''
            for ifile in files:
                image_html = image_html+'<img src="output/'+ ifile +'">'
            html = html + image_html

    return html

@app.route('/output/<name>')
def run_detector1(name):
    return send_file('output/'+name)

@app.route('/background_image/<name>')
def run_detector2(name):
    return send_file('background_image/'+name)

if __name__ == "__main__":
    app.run()
