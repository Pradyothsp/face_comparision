**Comparing the face of person with all the images in the dataset**

When a query image is given as input in the front end, ID of the person can be optained from the function `rec = cv2.face.LBPHFaceRecognizer_create()` and `rec.predict(image_array)`

When the ID of a person is obtained, all the images associated with the person ID can be obtained.