import glob
import os

import cv2
import numpy as np
import PIL as Image


FACE_DETECT = cv2.CascadeClassifier('recognizer/haarcascade_frontalface_default.xml')

rec = cv2.face.LBPHFaceRecognizer_create()
rec.read("recognizer/trainingData.yml")


def convert_to_gray(image_array):
    gray = cv2.cvtColor(image_array, cv2.COLOR_BGRA2GRAY)
    return gray


def detect_face_boundaries(image_array):
    faces = FACE_DETECT.detectMultiScale(image_array, 1.3, 5)
    return faces

def save_images(image_array, path):
    cv2.imwrite(path, image_array)
    return True

def crop_image(image_array, face):
    x, y, w, h = face
    image_croped_array = image_array[y:y+h, x:x+w]
    
    return image_croped_array

# QUARY_IMAGE_PATH = 'temp_folder'
files = os.listdir('temp_folder')
QUARY_IMAGE_PATH = os.path.join('temp_folder', files[0])
# QUARY_IMAGE_PATH = glob.glob('temp_folder/*.jpg')


def predect(QUARY_IMAGE_PATH):
    image_array = cv2.imread(QUARY_IMAGE_PATH)
    gray_image = convert_to_gray(image_array)
    faces = detect_face_boundaries(gray_image)
    croped_image = crop_image(gray_image, faces[0])

    id, conf = rec.predict(croped_image)

    return (id)


ID = predect(QUARY_IMAGE_PATH)

def show_images(ID = ID, dir_path = 'faces'):

    # images_path = glob.glob(dir_path+'/'+str(ID)+'/*')
    images_path = os.listdir(os.path.join(dir_path, str(ID)))

    for image_path in images_path:
        # image_array = cv2.imread(image_path)
        image_array = cv2.imread(os.path.join(dir_path, str(ID), image_path))
        # image_array = Image.open(os.path.join(dir_path, str(ID), image_path))
        image_array = cv2.resize(image_array, (150, 150), interpolation = cv2.INTER_NEAREST)
        # image_array = image_array.resize((200, 200))
        if image_array is not None:
            cv2.imshow(image_path, image_array)
            cv2.waitKey(1000)
            # save = save_images(image_array, os.path.join('output', image_path))
        
        elif image_array is None:
            print ("Error loading: " + image_path)
            continue

    return "File, Saved"

show_images(ID)