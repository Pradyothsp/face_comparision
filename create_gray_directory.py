import os
import sys

import cv2
import numpy as np
from PIL import Image

recognizer = cv2.face.LBPHFaceRecognizer_create()
FACE_DETECT = cv2.CascadeClassifier('recognizer/haarcascade_frontalface_default.xml')


path_to_save_gray_images = 'faces_gray'
dataset_path = 'faces'

try:
    os.mkdir(path_to_save_gray_images)

except:
    print("Folder already excists")


def convert_to_gray(image_array):
    gray = cv2.cvtColor(image_array, cv2.COLOR_BGRA2GRAY)
    return gray


def save_images(image_array, path):
    cv2.imwrite(path, image_array)
    return True


def detect_face_boundaries(image_array):
    gray = cv2.cvtColor(image_array, cv2.COLOR_BGRA2GRAY)
    faces = FACE_DETECT.detectMultiScale(gray,1.3,5)
    return faces


def crop_image(image_array, face):
    x, y, w, h = face
    image_croped_array = image_array[y:y+h, x:x+w]
    
    return image_croped_array


def copy_to_gray_folder(dir_path, path_to_save_gray_images):

    temp_count = 0

    folders_inside_dir_path = os.listdir(dir_path)

    for folders in folders_inside_dir_path:
        os.mkdir(os.path.join(path_to_save_gray_images, folders))
        files_inside_folder = os.listdir(os.path.join(dir_path, folders))

        for file_name in files_inside_folder:
            image_array = cv2.imread(os.path.join(dir_path,folders, file_name))
            faces = detect_face_boundaries(image_array)
            
            if len(faces) == 1:
                croped_image = crop_image(image_array, faces[0])
                save_image = save_images(convert_to_gray(croped_image), os.path.join(path_to_save_gray_images, folders, file_name))

            elif len(faces) == 0:
                print("face not found :{}".format(os.path.join(dir_path, folders, file_name)))
                pass

            elif len(faces) > 1:
                for face in faces:
                    croped_image1 = crop_image(image_array, face)
                    save_image = save_images(convert_to_gray(croped_image1), "temp/"+str(temp_count)+"::"+file_name)

                    temp_count += 1

    return True

status = copy_to_gray_folder(dataset_path, path_to_save_gray_images)

if status:
    sys.exit()
