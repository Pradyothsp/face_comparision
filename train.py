import os
import glob
import cv2
import numpy as np 
from PIL import Image 

recognizer = cv2.face.LBPHFaceRecognizer_create()
data_path = 'faces_gray'

def getImageWithID(data_path):    
    folders = os.listdir(data_path)
    faces=[]
    IDs=[]

    for folder in folders:
        imagePaths = os.listdir(os.path.join(data_path,folder))

        for image in imagePaths:
            imagePath = os.path.join(data_path, folder, image)
            faceImg = Image.open(imagePath).convert('L')
            faceNp = np.array(faceImg,'uint8')
            ID = int(float(folder))
            faces.append(faceNp)
            IDs.append(ID)
            
    return np.array(IDs), faces

Ids,faces = getImageWithID(data_path)
# print(Ids)
# print(type(Ids))
# print(type(faces[0]))
recognizer.train(faces,Ids)
recognizer.save("recognizer/trainingData.yml")